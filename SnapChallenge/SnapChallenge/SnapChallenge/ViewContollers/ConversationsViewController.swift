//
//  ConversationsViewController.swift
//  SnapChallenge
//
//  Created by Andreas Grøntved Jeppesen on 08/11/2018.
//  Copyright © 2018 ANDREAS G. JEPPESEN. All rights reserved.
//

import UIKit

class ConversationsViewController: UIViewController {
    
    var conversations:[Conversation] = []
    var posts:[Post] = []
    var data:DataLayer? = nil
    var isConversationMode = true
    var index = 0
    
    @IBOutlet weak var challengeTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        data = DataLayer()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(ConversationsViewController.back))
        
        getAndSetConversations()
        
    }
    
    
    @objc func back() {
        print("you feel me")
        if isConversationMode {
            self.navigationController?.popViewController(animated: true)
        } else {
            isConversationMode=true
            getAndSetConversations()
        }
    }
    
    func getAndSetPosts(index:Int = -1){
        if index > -1{
            self.index = index
        }
        posts = conversations[self.index].posts
        if !isConversationMode{
        self.challengeTableView.delegate = self
        self.challengeTableView.dataSource = self
        self.challengeTableView.reloadData()
        }
    }
    
    func getAndSetConversations(){
        data?.getConversationsOfUser(userName: UserDefaults.standard.string(forKey: "userName")!, completion: { (conversationIds) in
            
            
            self.conversations = []
            conversationIds.forEach({ (id) in
                self.data?.getConversationFromID(conversationId: id, completion: { (conversation) in
                    self.conversations.append(conversation)
                    
                    if !self.conversations.isEmpty || self.index<self.conversations.count {
                        self.posts = self.conversations[self.index].posts
                    }
                
                    self.getAndSetPosts()
                    self.challengeTableView.delegate = self
                    self.challengeTableView.dataSource = self
                    self.challengeTableView.reloadData()
                })
            })
        })
    }
    
}
extension ConversationsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return   isConversationMode ?  conversations.count :posts.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isConversationMode {
            isConversationMode = false
            index = indexPath.row
            getAndSetPosts(index: indexPath.row)
        } else {
            let vc: InviteViewController? = self.storyboard?.instantiateViewController(withIdentifier: "InviteViewController") as? InviteViewController
            if let validVC: InviteViewController = vc {
                validVC.posts = [self.posts[indexPath.row]]
                validVC.rate = true
                self.navigationController?.pushViewController(validVC, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: "conversationCell") as! ConversationViewCell
        
        if(isConversationMode){
            let conversation = conversations[indexPath.row]
            let textRight = "Posts: \(conversation.posts.count)" //conversation.participants.joined(separator: ", ")
            let textLeft = conversation.participants.joined(separator: ", ") //conversation.id
            cell.setData(textLeft: textLeft, textRight: textRight)
        } else {
            let post = posts[indexPath.row]
            let textLeft = post.infoText
            let textRight = "\(post.rating)"
            cell.setData(textLeft: textLeft, textRight: textRight)
        }
        return cell
    }
}

