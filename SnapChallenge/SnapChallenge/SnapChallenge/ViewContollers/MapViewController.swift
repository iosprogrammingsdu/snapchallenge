//
// Group 17 - ajepp09, emije14, nije214
// SnapChallenge
// iOS Programmering - Fall semester 2018
//

import UIKit
import CoreLocation
import MapKit

class MapViewController: UIViewController {
    
    var latitude : Double = 55
    var longitude : Double = 10
    @IBOutlet weak var map: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let center = CLLocationCoordinate2DMake(latitude, longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.3,longitudeDelta: 0.3)
        let mapRegion = MKCoordinateRegion(center: center, span: span)
        let marker = MKPlacemark(coordinate: center)
        self.map.setRegion(mapRegion, animated: true)
        self.map.addAnnotation(marker)
    }
}
