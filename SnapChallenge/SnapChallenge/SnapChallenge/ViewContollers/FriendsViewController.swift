//
// Group 17 - ajepp09, emije14, nije214
// SnapChallenge
// iOS Programmering - Fall semester 2018
//

import UIKit


class FriendsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var users: [User] = []
    var data:DataLayer?
    var currentUser:User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        data = DataLayer()
        
        data?.logIn(userName: UserDefaults.standard.string(forKey: "userName")!, password: UserDefaults.standard.string(forKey: "password")!, completion: { (user) in
            self.data!.getUsersByUserNames(userNames: user.friends, completion: { (friends) in
                self.users = friends
                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.tableView.reloadData()
            })
        })
    }
}

extension FriendsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let user = users[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsListCell") as! FriendsListCell
        cell.setUser(user: user)
        return cell
    }
}
