//
// Group 17 - ajepp09, emije14, nije214
// SnapChallenge
// iOS Programmering - Fall semester 2018
//

import UIKit
import CoreLocation

class ChallengeViewController: UIViewController, UITextViewDelegate, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    let placeholderText : String = "Write a challenge!"
    var latitude : Double = 55
    var longitude : Double = 10
    
    @IBOutlet weak var descriptionTextField: UITextView!
    var previewImage : UIImage?
    
    @IBOutlet weak var imagePreview: UIImageView!
    
    @IBAction func sendButtonUp(_ sender: Any) {
        challenge()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePreview.image = previewImage
        descriptionTextField.delegate = self
        descriptionTextField.text = placeholderText
        descriptionTextField.textColor = UIColor.lightGray
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeholderText
            textView.textColor = UIColor.lightGray
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if(locationManager.location != nil) {
            latitude = manager.location!.coordinate.latitude
            longitude = manager.location!.coordinate.longitude
        }
    }
    
    func challenge() {
        let vc: ChallengeFriendsTableViewController? = self.storyboard?.instantiateViewController(withIdentifier: "ChallengeFriendsTableViewController") as? ChallengeFriendsTableViewController
        if let validVC: ChallengeFriendsTableViewController = vc {
            validVC.post = Post.init(author: UserDefaults.standard.string(forKey: "userName")!, image: previewImage!.pngData()!, infoText: descriptionTextField.text, latitude: self.latitude, longitude: self.longitude)
            self.navigationController?.pushViewController(validVC, animated: true)
        }
    }
}
