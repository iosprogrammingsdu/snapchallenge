//
// Group 17 - ajepp09, emije14, nije214
// SnapChallenge
// iOS Programmering - Fall semester 2018
//

import Foundation
import UIKit

class RegisterViewController : UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    let placeholderText:String = "Username"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTextField.delegate = self
        usernameTextField.text = placeholderText
        usernameTextField.textColor = UIColor.lightGray
        passwordTextField.delegate = self
    }
    
    
    @IBAction func beginEditing(_ sender: Any) {
        if usernameTextField.textColor == UIColor.lightGray {
            usernameTextField.text = ""
            usernameTextField.textColor = UIColor.black
        }
    }
    
    @IBAction func editingEndedWithNoText(_ sender: Any) {
        if usernameTextField.text == nil {
            usernameTextField.text = placeholderText
            usernameTextField.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func registerUser(_ sender: Any) {
        register(username: usernameTextField.text!, password: passwordTextField.text!)
    }
    
    var data:DataLayer? = nil
    func register(username:String, password:String){
        data = DataLayer()
        if password.count < 3{
            return
        }
        data?.getUsersByUserNames(userNames: [username], completion: { (users) in
            if !users.isEmpty {
                return
            }
            else {
                self.data?.createUser(userName: username, password: password)
            }
        })
    }
}
