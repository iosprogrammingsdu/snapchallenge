//
// Group 17 - ajepp09, emije14, nije214
// SnapChallenge
// iOS Programmering - Fall semester 2018
//

import Foundation
import UIKit

class LoginViewController: UIViewController {
    
    @IBAction func LoginButtonUp(_ sender: Any) {
        login()
    }
    
    @IBOutlet weak var PasswordTextView: UITextField!
    @IBOutlet weak var UsernameTextView: UITextField!
    
    override func viewDidLoad() {
     super.viewDidLoad()
     }
    
    override func viewDidLayoutSubviews() {
        if(UserDefaults.standard.string(forKey: "userName") != nil) {
            toMainView()
        }
    }
    
    func login() {
        let data = DataLayer.init()
        guard let userName = UsernameTextView.text 	else {return}
        guard let password = PasswordTextView.text else {return}
        data.logIn(userName: userName, password: password, completion: { (user) in
            if (user.userName == self.UsernameTextView.text!) {
                UserDefaults.standard.set(userName, forKey: "userName")
                UserDefaults.standard.set(password, forKey: "password")
                self.toMainView()
            }
        })
    }
    
    func toMainView() {
        let vc: UINavigationController? = self.storyboard?.instantiateViewController(withIdentifier: "NavigationController") as? UINavigationController
        if let validVC: UINavigationController = vc {
            self.present(validVC, animated: true)
        }
    }
}
