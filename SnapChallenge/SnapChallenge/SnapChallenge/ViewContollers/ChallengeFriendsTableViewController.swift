//
// Group 17 - ajepp09, emije14, nije214
// SnapChallenge
// iOS Programmering - Fall semester 2018
//

import UIKit

class ChallengeFriendsTableViewController: UITableViewController {
    
    var users: [User] = []
    var data : DataLayer = DataLayer()
    var currentUser:User?
    var post : Post?
    
    @IBOutlet var friendsTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        data = DataLayer()
        data.logIn(userName: UserDefaults.standard.string(forKey: "userName")!, password: UserDefaults.standard.string(forKey: "password")!, completion: { (user) in
            self.data.getUsersByUserNames(userNames: user.friends, completion: { (friends) in
                self.users = friends
                self.currentUser = user
                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.tableView.reloadData()
            })
        })
        self.tableView.allowsMultipleSelection = true
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ChallengeFriendTableViewCell", for: indexPath) as? ChallengeFriendTableViewCell else  {
            fatalError("Viewcells could not be loaded")
        }
        cell.FriendNameTextView.text = users[indexPath.row].userName
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    @IBAction func ChallengeButtonTouchUp(_ sender: Any) {
        for friend in getSelectedFriends() {
            data.getConversationsParticipants(userName: currentUser!.userName) { (participants) in
                if participants.contains(friend) {
                    self.data.getConversationFromParticipant(participant: friend, completion: { (conversation) in
                    self.data.addPostToConversation(conversation: conversation, post: self.post!)
                    })
                } else {
                    let conversation = Conversation.init(posts: [self.post!], id:UUID().uuidString, participants: [friend])
                    self.data.createConversation(conversation: conversation)
                }
            }
        }
        toMainView()
    }
    
    func getSelectedFriends() -> [String]{
        let indexes = self.tableView.indexPathsForSelectedRows
        var friends = [String]()
        if(indexes != nil) {
            for path in indexes! {
                let currentCell = tableView.cellForRow(at: path) as! ChallengeFriendTableViewCell
                friends.append(currentCell.FriendNameTextView.text!)
            }
        }
        return friends
    }
    
    func toMainView() {
        let vc: ViewController? = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
        if let validVC: ViewController = vc {
            self.navigationController?.pushViewController(validVC, animated: true)
        }
    }
}
