//
//  ConversationsViewController.swift
//  SnapChallenge
//
//  Created by Andreas Grøntved Jeppesen on 08/11/2018.
//  Copyright © 2018 ANDREAS G. JEPPESEN. All rights reserved.
//

import UIKit

class AddFriendViewController: UIViewController {
    let data : DataLayer = DataLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func AddButtonTouchUp(_ sender: Any) {
        data.getUsersByUserNames(userNames: [UserDefaults.standard.string(forKey: "userName")!, FriendNameTextView.text!.trimmingCharacters(in: .whitespacesAndNewlines)]) { (users) in
            if(users.count < 2) {
                self.ErrorLabel.isHidden = false
                self.ErrorLabel.text! = "Friend Does not exist"
                return
            }
            self.data.addFriend(friendUserName: users[1].userName, toUser: users[0])
            self.data.addFriend(friendUserName: users[0].userName, toUser: users[1])
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBOutlet weak var ErrorLabel: UILabel!
    @IBOutlet weak var FriendNameTextView: UITextField!
}
