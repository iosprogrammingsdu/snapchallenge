//
// Group 17 - ajepp09, emije14, nije214
// SnapChallenge
// iOS Programmering - Fall semester 2018
//

import UIKit
import AVFoundation
import CoreLocation

class ViewController: UIViewController, AVCapturePhotoCaptureDelegate, CLLocationManagerDelegate {
    
    var captureSession: AVCaptureSession!
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    @IBOutlet weak var ChallengesButton: UIButton!
    let locationManager = CLLocationManager()
    var posts : [Post] = []
    
    var data:DataLayer? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        data = DataLayer()
        self.navigationItem.hidesBackButton = true
        //deal with notifications here
        locationManager.requestAlwaysAuthorization()
        ChallengesButton.backgroundColor = UIColor.blue
        ChallengesButton.titleLabel?.text = "0"
        let userName = UserDefaults.standard.string(forKey: "userName")!
        data?.getConversationsOfUser(userName: userName, completion: { (conversations)  in
            for id in conversations {
                self.data?.getPostsOfConversation(conversationId: id, completion: {(conversationPosts) in
                    let filtered = conversationPosts.filter({(post) in
                         !post.acceptedBy.contains(userName) && !post.rejectedBy.contains(userName)
                    })
                    self.posts.append(contentsOf: filtered)
                    self.ChallengesButton.titleLabel!.text = self.posts.count.description
                    if(self.posts.count > 0) {
                     self.ChallengesButton.backgroundColor = UIColor.red
                    }
                })
            }
        })
    }
    
    @IBAction func challengesButtonTouchUp(_ sender: Any) {
        if(posts.count == 0) {
            return
        }
        let vc: InviteViewController? = self.storyboard?.instantiateViewController(withIdentifier: "InviteViewController") as? InviteViewController
        if let validVC: InviteViewController = vc {
            validVC.posts = self.posts
            self.navigationController?.pushViewController(validVC, animated: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.ChallengesButton.titleLabel!.text = self.posts.count.description
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .medium
        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
            else {
                return
        }
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            stillImageOutput = AVCapturePhotoOutput()
            if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                captureSession.addInput(input)
                captureSession.addOutput(stillImageOutput)
                setupLivePreview(mode: .portrait)
            }
            
        }
        catch let error  {
        }
    }
    
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let imageData = photo.fileDataRepresentation()
            else { return }
        
        challenge(img: UIImage(data: imageData)!)
    }
    
    func challenge(img: UIImage) {
        let vc: ChallengeViewController? = self.storyboard?.instantiateViewController(withIdentifier: "ChallengeViewController") as? ChallengeViewController
        if let validVC: ChallengeViewController = vc {
            validVC.previewImage = img
            self.navigationController?.pushViewController(validVC, animated: true)
        }
    }
    
    
    @IBAction func HistoryClick(_ sender: UIButton) {
        let vc: ConversationsViewController? = self.storyboard?.instantiateViewController(withIdentifier: "ConversationViewController") as? ConversationsViewController
        if let validVC: ConversationsViewController = vc {
            self.navigationController?.pushViewController(validVC, animated: true)
        }
    }
    
    
    @IBOutlet weak var imgView: UIImageView!
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.captureSession.stopRunning()
    }
    
    @IBOutlet weak var previewView: UIView!
    @IBAction func ChallengeButtonTouchUp(_ sender: Any) {
        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        if stillImageOutput == nil {
            challenge(img: UIImage(named: "cross")!)
        } else {
            stillImageOutput.capturePhoto(with: settings, delegate: self)
        }
    }
    func setupLivePreview(mode : AVCaptureVideoOrientation) {
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.connection?.videoOrientation = mode
        previewView.layer.addSublayer(videoPreviewLayer)
        DispatchQueue.global(qos: .userInitiated).async {
            self.captureSession.startRunning()
            DispatchQueue.main.async {
                self.videoPreviewLayer.frame = self.previewView.bounds
            }
        }
        
    }
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        var mode = AVCaptureVideoOrientation.portrait
        switch UIDevice.current.orientation{
        case .portrait:
            mode = .portrait
        case .portraitUpsideDown:
            mode = .portraitUpsideDown
        case .landscapeLeft:
            mode = .landscapeLeft
        case .landscapeRight:
            mode = .landscapeRight
        default:
            mode = .portrait
        }
        self.captureSession.stopRunning()
        setupLivePreview(mode:mode)
    }
}

