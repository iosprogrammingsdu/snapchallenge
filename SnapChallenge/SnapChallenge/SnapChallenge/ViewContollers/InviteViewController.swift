//
// Group 17 - ajepp09, emije14, nije214
// SnapChallenge
// iOS Programmering - Fall semester 2018
//

import Foundation
import UIKit

class InviteViewController: UIViewController {
    let data : DataLayer = DataLayer()
    var posts : [Post] = []
    var index : Int = 0
    @IBOutlet weak var DeclineButton: UIButton!
    @IBOutlet weak var AcceptButton: UIButton!
    @IBOutlet weak var RateButton: UIButton!
    var rate : Bool = false
    @IBOutlet weak var Stepper: UIStepper!
    @IBAction func RateButtonTouchUp(_ sender: Any) {
        handleRating()
    }
    @IBAction func StepperValueChanged(_ sender: Any) {
        if Stepper.value > 10 {
            Stepper.value = 10
        }
        if Stepper.value < 1 {
            Stepper.value = 1
        }
        RatingView.text! = "\(Int(Stepper.value))"
    }
    @IBOutlet weak var RatingView: UITextField!
    
    @IBOutlet weak var authorLabel: UILabel!
    
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var DescriptionTextField: UITextView!
     
    @IBAction func AcceptButtonUp(_ sender: Any) {
        var post = posts[index]
        post.acceptedBy.append(UserDefaults.standard.string(forKey: "userName")!)
        data.updatePost(post: post)
        nextOrBack()
    }
    
    
    @objc func back() {
        print("you feel me")
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.viewControllers.forEach({ (view) in
            if let con = view as? ConversationsViewController{
                con.getAndSetConversations()
            }
        })
        
      
    }
    
    
    @IBAction func DeclineButtonUp(_ sender: Any) {
        var post = posts[index]
        post.rejectedBy.append(UserDefaults.standard.string(forKey: "userName")!)
        data.updatePost(post: post)
        nextOrBack()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        handleUI(post: posts[0])
         navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(InviteViewController.back))
    }
    
    func nextOrBack() {
        index+=1
        if(index >= posts.count) {
            let vc: ViewController? = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
            if let validVC: ViewController = vc {
                self.navigationController?.pushViewController(validVC, animated: true)
            }
        } else {
            handleUI(post:posts[index])
        }
    }
    func handleUI(post: Post) {
        if rate {
            RateButton.isHidden = false
            Stepper.isHidden = false
            RatingView.isHidden = false
            DeclineButton.isHidden = true
            AcceptButton.isHidden = true
            
        }
        image.image = UIImage(data: post.image)
        DescriptionTextField.text = post.infoText
        authorLabel.text = post.author
    }
    @IBAction func mapButtonTouchUp(_ sender: Any) {
        let vc: MapViewController? = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as? MapViewController
        if let validVC: MapViewController = vc {
            validVC.latitude = self.posts[index].latitude
            validVC.longitude = self.posts[index].longitude
            self.navigationController?.pushViewController(validVC, animated: true)
        }
    }
    
    func handleRating() {
        let diff = Int(Stepper.value) - posts[index].rating
        posts[index].rating = Int(Stepper.value)
        data.getUsersByUserNames(userNames: [UserDefaults.standard.string(forKey: "userName")!]) { (user) in
            self.data.increaseScore(user: user[0], scoreIncrease: diff)
        }
        data.updatePost(post: posts[index])
    }
}
