//
// Group 17 - ajepp09, emije14, nije214
// SnapChallenge
// iOS Programmering - Fall semester 2018
//

import UIKit

class FriendsListCell: UITableViewCell {
    
    @IBOutlet weak var nameView: UILabel!
    
    @IBOutlet weak var scoreView: UILabel!
    
    func setUser(user: User){
        nameView.text = user.userName
        scoreView.text = "\(user.score)"
    }
}
