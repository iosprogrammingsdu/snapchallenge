//
//  ConversationViewCell.swift
//  SnapChallenge
//
//  Created by Andreas Grøntved Jeppesen on 08/11/2018.
//  Copyright © 2018 ANDREAS G. JEPPESEN. All rights reserved.
//

import UIKit

class ConversationViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   
    @IBOutlet weak var convId: UITextField!
    
    @IBOutlet weak var users: UITextField!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    func setData(textLeft:String,textRight:String){
        users.text = textRight
        convId.text = textLeft
    }
}
