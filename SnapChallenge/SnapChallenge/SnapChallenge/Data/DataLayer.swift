//
// Group 17 - ajepp09, emije14, nije214
// SnapChallenge
// iOS Programmering - Fall semester 2018
//

import Foundation
import Firebase
import FirebaseDatabase

class DataLayer {
    
    let ref: DatabaseReference!
    
    init() {
        ref = Database.database().reference()
    }
    
    func getConversationsOfUser(userName:String,completion: @escaping (_ conversationIDs:Array<String>)->()){
        ref.child("Conversations").queryOrdered(byChild: "authors").observe (.value) { (snapshot) in
            var ids = Array<String>()
            for snap in snapshot.children {
                let snapshotValue = (snap as! DataSnapshot).value as! [String: AnyObject]
                let authors:Array<String> = (snapshotValue["authors"] as! String).components(separatedBy: ",")
                let id:String = snapshotValue["id"] as! String
                authors.filter({ (auth) -> Bool in
                    auth == userName
                }).forEach({ (auth) in
                    ids.append(id)
                })
            }
            completion(ids)
        }
    }
    
    func getConversationsParticipants(userName:String,completion: @escaping (_ conversationIDs:Array<String>)->()){
        ref.child("Conversations").queryOrdered(byChild: "authors").observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            var ids = Array<String>()
            for snap in snapshot.children {
                let snapshotValue = (snap as! DataSnapshot).value as! [String: AnyObject]
                let authors:Array<String> = (snapshotValue["authors"] as! String).components(separatedBy: ",")
                ids.append(contentsOf: authors)
            }
            completion(ids)
        })
    }
    
    
    func updatePost(post:Post){
        ref.child("Posts").child(post.id).setValue([ "author": post.author,
                                                     "conversation_id": post.conversation_id,
                                                     "rejectedBy": post.rejectedBy,
                                                     "acceptedBy": post.acceptedBy,
                                                     "image": post.image.base64EncodedString(options: .lineLength64Characters),
                                                     "info_text": post.infoText,
                                                     "rating": post.rating,
                                                     "time": post.time,
                                                     "latitude": post.latitude,
                                                     "longitude": post.longitude,
                                                     "id": post.id]
        )
    }
    
    func logIn(userName:String, password:String,completion: @escaping (_ user:User)->()){
        ref.child("Users").queryOrdered(byChild: "userName")
            .queryEqual(toValue: userName).observe (.value) { (snapshot) in
                snapshot.children.forEach({ (snap) in
                    let convSnap = (snap as! DataSnapshot).value as! [String: AnyObject]
                    //10 ud af 10 sikkerhed
                    let dbPassword:String = convSnap ["password"] as! String
                    let dbScore: Int = convSnap ["score"] as! Int
                    let dbFriends: String = convSnap ["friends"] as! String
                    let dbId: String = (snap as! DataSnapshot).key
                    if password == dbPassword{
                        completion(User(userName: userName, score: dbScore, friends: dbFriends.components(separatedBy: ","), id: dbId))
                    } else {
                        completion(User(userName: "failure", score: 0, friends:[],id:"noid"))
                    }
                    return
                })
                completion(User(userName: "failure", score: 0, friends:[],id:"noid"))
        }
    }
    
    
    func createUser(userName:String, password:String) ->User {
        let id = UUID().uuidString
        ref.child("Users")
            .childByAutoId().setValue(
                [ "friends": "",
                  "userName": userName,
                  "score": 0,
                  "password": password]
        )
        return User(userName: userName, score:0, friends:[],id:id )
    }
    
    func addPostToConversation(conversation:Conversation, post:Post)->Conversation {
        let id = UUID().uuidString
        conversation.posts.append(Post(author: post.author, image: post.image, infoText: post.infoText, rating: post.rating, time: post.time,latitude: post.latitude, longitude: post.longitude, id: id, acceptedBy:[], rejectedBy:[], conversation_id:conversation.id))
        ref.child("Posts").child(id)
            .setValue(
                [ "author": post.author,
                  "image": post.image.base64EncodedString(options: .lineLength64Characters)    ,
                  "info_text": post.infoText,
                  "rating": post.rating,
                  "conversation_id": conversation.id,
                  "time": post.time,
                  "latitude": post.latitude,
                  "longitude": post.longitude,
                  "acceptedBy":post.acceptedBy.joined(separator: ","),
                  "rejectedBy":post.rejectedBy.joined(separator: ","),
                  ])
        
        return conversation
    }
    
    func createConversation(conversation:Conversation) -> Conversation {
        let id = conversation.id
        ref.child("Conversations").child(id).setValue(
            [ "authors": conversation.participants.joined(separator: ","),
              "id": id]
        )
        conversation.posts.forEach { (post) in
            addPostToConversation(conversation: conversation, post: post)
        }
        return Conversation(posts: conversation.posts, id: id, participants: conversation.participants)
    }
    
    
    func getConversationFromID(conversationId:String,completion: @escaping (_ conversation:Conversation)->())      {
        ref.child("Conversations").queryOrdered(byChild: "id")
            .queryEqual(toValue: conversationId).observe (.value) { (snapshot) in
                snapshot.children.forEach({ (snap) in
                    let convSnap = (snap as! DataSnapshot).value as! [String: AnyObject]
                    let authors:Array<String> = (convSnap["authors"] as! String).components(separatedBy: ",")
                    
                    self.getPostsOfConversation(conversationId: conversationId,completion:
                        { (posts) in
                            completion(Conversation(posts: posts,id:conversationId,participants: authors))
                    })
                    //Antag kun een med ID, refaktor senere
                    return
                })
        }
    }
    
    func getConversationFromParticipant(participant:String,completion: @escaping (_ conversation:Conversation)->())      {
        ref.child("Conversations").queryOrdered(byChild: "authors")
            .queryEqual(toValue: participant).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
                snapshot.children.forEach({ (snap) in
                    let convSnap = (snap as! DataSnapshot).value as! [String: AnyObject]
                    let authors:Array<String> = (convSnap["authors"] as! String).components(separatedBy: ",")
                    
                    self.getPostsOfConversation(conversationId: participant,completion:
                        { (posts) in
                            completion(Conversation(posts: posts, id:(snap as! DataSnapshot).key, participants: authors))
                    })
                    //Antag kun een med ID, refaktor senere
                    return
                })
        })
    }
    
    func getPostsOfConversation(conversationId:String,completion: @escaping (_ posts:Array<Post>)->()){
        ref.child("Posts").queryOrdered(byChild: "conversation_id")
            .queryEqual(toValue: conversationId).observe (.value) { (snapshot) in
                let posts = snapshot.children.map({ (snapshot) -> Post in
                    Post(snapshot: snapshot as! DataSnapshot,id: (snapshot as! DataSnapshot).key)
                })
                completion(posts)
        }
    }
    
    func addFriend(friendUserName:String, toUser:User){
        toUser.friends.append(friendUserName)
        ref.child("Users").child(toUser.id).updateChildValues(
            [ "friends": toUser.friends.joined(separator: ",")]
        )
    }
    
    func increaseScore(user:User, scoreIncrease: Int){
        ref.child("Users").child(user.id).updateChildValues(["score": user.score + scoreIncrease])
    }
    
    func getUsersByUserNames(userNames:[String],completion: @escaping (_ posts:Array<User>)->()){
        var users:[User?] = []
        for i in 0..<userNames.count {
            ref.child("Users").queryOrdered(byChild: "userName")
                .queryEqual(toValue: userNames[i]).observe (.value) { (snapshot) in
                    let user = snapshot.children.map({ (snap) -> User in
                        let convSnap = (snap as! DataSnapshot).value as! [String: AnyObject]
                        let dbScore: Int = convSnap ["score"] as! Int
                        let dbFriends: String = convSnap ["friends"] as! String
                        let dbId: String = (snap as! DataSnapshot).key
                        let userName = convSnap ["userName"] as! String
                        return User(userName: userName, score: dbScore, friends: dbFriends.components(separatedBy: ","), id: dbId)
                    }).first
                    if user == nil {
                        users.append(nil)
                    }else{
                        users.append(user!)
                    }
                    if users.count == userNames.count{
                        completion(users.filter({ (user) -> Bool in
                            user != nil
                        }).map({ (user) -> User in
                            user!
                        }))
                    }
            }
        }
    }
}
