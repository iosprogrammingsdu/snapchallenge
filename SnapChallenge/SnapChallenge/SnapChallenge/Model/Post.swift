//
// Group 17 - ajepp09, emije14, nije214
// SnapChallenge
// iOS Programmering - Fall semester 2018
//

import Foundation
import Firebase
import FirebaseDatabase

struct Post{
    let author:String
    let image:Data
    let infoText:String
    let time:Int64
    var rating:Int
    let latitude: Double
    let longitude: Double
    let id:String
    var acceptedBy:[String]
    var rejectedBy:[String]
    let conversation_id:String
    
    init(author:String, image:Data, infoText:String,rating:Int = 0,time:Int64 =  Int64(NSDate().timeIntervalSince1970 * 1000),latitude: Double, longitude: Double, id:String = "void", acceptedBy:[String]=[], rejectedBy:[String]=[], conversation_id:String="") {
        self.author = author
        self.image = image
        self.infoText = infoText
        self.rating = rating
        self.time = time
        self.latitude = latitude
        self.longitude = longitude
        self.id = id
        self.acceptedBy = acceptedBy
        self.rejectedBy = rejectedBy
        self.conversation_id = conversation_id
    }
    
    init(snapshot: DataSnapshot, id:String) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        author = snapshotValue["author"] as! String
        image =  Data(base64Encoded: (snapshotValue["image"] as! String), options: .ignoreUnknownCharacters)!        
        infoText = snapshotValue["info_text"] as! String
        rating = snapshotValue["rating"] as! Int
        time = snapshotValue["time"] as! Int64
        latitude = snapshotValue["latitude"] as! Double
        longitude = snapshotValue["longitude"] as! Double
        self.id = id
        acceptedBy = (snapshotValue["acceptedBy"] as? [String]) ??  [] //??  []
        rejectedBy = (snapshotValue["rejectedBy"] as? [String]) ??  [] //??  []
        conversation_id = snapshotValue["conversation_id"] as! String
    }
}
