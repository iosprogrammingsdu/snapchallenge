//
// Group 17 - ajepp09, emije14, nije214
// SnapChallenge
// iOS Programmering - Fall semester 2018
//

import Foundation

class User{
    let userName:String
    var score:Int
    var friends:Array<String>
    var id:String
    
    init(userName:String, score:Int,friends:Array<String>,id:String) {
        self.userName = userName
        self.score = score
        self.friends = friends
        self.id = id
    }
}
