//
// Group 17 - ajepp09, emije14, nije214
// SnapChallenge
// iOS Programmering - Fall semester 2018
//

import Foundation

class Conversation{
    var posts:Array<Post>
    let id:String
    let participants:Array<String>
    
    init(posts:Array<Post> = [],id:String="hest", participants:Array<String> = []) {
        self.posts = posts
        self.participants = participants
        self.id = id
    }
}
